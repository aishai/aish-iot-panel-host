import { NgModule } from '@angular/core';
import { 
  MatIconModule, 
  MatInputModule, 
  MatListModule, 
  MatFormField, 
  MatSelectModule,
  MatOptionModule,
  MatToolbarModule,
  MatRadioModule,
  MatExpansionModule,
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  imports: [
    BrowserAnimationsModule, 
    MatIconModule, 
    MatInputModule, 
    MatListModule,
    MatSelectModule,
    MatOptionModule,
    MatToolbarModule,
    MatRadioModule,
    MatExpansionModule
  ],
  exports: [
    BrowserAnimationsModule, 
    MatIconModule, 
    MatInputModule, 
    MatListModule,
    MatSelectModule,
    MatRadioModule,
    MatOptionModule,
    MatToolbarModule,
    MatExpansionModule
  ]
})
export class MaterialModule { }
