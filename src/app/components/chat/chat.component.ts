/// <reference path="./../../../../typings/compromise.d.ts" />

import { Component, OnInit, OnDestroy, AfterViewChecked, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { client } from './../../dialog-flow-client/dialog-flow.client';
import { IMessage } from './../../models/message';
import { excuses } from './../../models/excuses'
import Artyom from 'artyom.js/build/artyom.js'
import * as nlp  from 'compromise'

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  // TODO: PanelSettings object
  conversation: IMessage[] = [];

  wakeWords: {alexia:string,google:string} = {
    alexia: 'Amazon',
    google: 'Hey Google'
  }
  speaker: string = this.wakeWords.alexia

  forcedSpeaker: string = 'Automatic'

  continouslyAskedQuestions: number

  isSpeaking: boolean;

  aish: Artyom

  userDictation: Artyom.Dictation

  fallbackExcuses: string[] = excuses
  // store those indexes used before to prevent duplicates
  useFallbackExcuses: number[] = []

  debugOpenState: boolean = false

  prefixPhrases: string[] = [
    'Very well.',
    'Thanks for your honest answer.',
    'I am delighted.',
    'Ho ho, that is a very vague thing to say.',
    'I respect your opinion.',
    'Well, no but yes.',
    'Of course,',
    'On the other hand, what would you say? ',
    'I assume so, but in maybe put it into your own words.',
    'Yes!',
    'Erm',
    'No!',
    '',
    '',
    '',
    '',
    '',
    ''
  ]

  /**
   * Once the idle time expires Aish will try to rephrase the question and than fire of another topic if the answer does not qualify fit for the
   * conversation.
   */
  idleTime: number = 3;
  idleTimer: number;

  repetitionsCount: number = 1
  /**
   * An array of possible times a certain device is addressed in a sequence before Aish switches automatically to the other
   */
  repetitionOptions: number[] = [1,3,5] 

  
  
  /**
   * 
   * @param ref 
   */
  constructor(
    private ref: ChangeDetectorRef
  ) {

  }


  ngOnInit() {
    this.ref.markForCheck();

    // Speaking initialize
    let initObj = {
      speed: 1,
      lang: "en-GB",
      debug: true,
      listen: true // Start to listen commands !
    };
    this.aish = new Artyom(initObj)
    this.aish.ArtyomVoicesIdentifiers["en-GB"] = ["Google UK English Male", "Google UK English Female", "en-GB", "en_GB"];

    this.startDictation()

  }

  ngOnDestroy() {
    this.userDictation.stop()
  }

  /**
   * Start a fresh dictation
   */
  startDictation() {
    this.userDictation = this.aish.newDictation({
      continuous: true, // Enable continuous if HTTPS connection
      onResult: (interim, temporal) => {
        console.log(interim, this.isSpeaking)
        if (temporal) {
          if (!this.isSpeaking) {
            this.addMessageFromDictation(temporal)
            console.log("Final recognized text: " + temporal);
          } else {
            console.log('currently speaking ... please re-phrase your question in a second, wait!')
          }
        }

      },
      onStart: () => {
        console.log('listening again!')
      },
      onEnd: () => {
        console.log('ended the dictation')
      }
    });
    // Dicatation initialize
    this.userDictation.start()

    // TODO: Idle timer to rephrase the question or start a new topic. or move on.
    let idle = () => {
      setTimeout(() => {
        this.idleTime
        idle()
      }, 1000)
    }
  }

  addMessageFromDictation(text) {
    if (text != '') {

      this.conversation.push({
        avatar: (this.speaker != this.wakeWords.alexia) ? 'home' : 'radio_button_unchecked',
        from: this.indentifySpeaker(),
        content: text
      });

      // Get response from API
      client.textRequest(text).then((response) => {

        this.speak(response.result.fulfillment['speech'])


      });
    }
  }

  /**
   * Adds a message from the chat client terminal 
   * @param message 
   * 
   */
  addMessageFromUser(message) {

    if (message.value != '') {

      // Post user message to chat window
      this.conversation.push({
        avatar: 'perm_identity',
        from: 'Me',
        content: message.value
      });
      // Get response from API
      client.textRequest(message.value).then((response) => {
        message.value = ''
        this.speak(response.result.fulfillment['speech'])
      });
    }
  }

  /**
   * 
   * @param text Do the actual speaking
   * @param assumptionByAish assume that Aish is trying to make a statement
   */
  speak(text: string, assumptionByAish?: boolean) {
    if(!text){
      assumptionByAish = true
      text = this.getExcuse()
    } else {
      text = this.orchestrateToSpeakers(text)
    }
    
    this.conversation.push({
      avatar: 'change_history',
      from: 'Aish',
      content: text
    });
    // Refresh view because answers come from outside the angular scope of watched variables
    this.ref.detectChanges()
    this.ref.markForCheck();
    // Stop dictation before speaking anything.
    this.userDictation.stop()
    this.aish.ArtyomWebkitSpeechRecognition.abort();
    // If aish is trying to make a point, do not orchestrate the question to any speaker.
    this.aish.say(text, {
      onEnd: () => {
        this.isSpeaking = false
        
        this.startDictation()
      }

    })

  }

  /**
   * Remember the last speaker that was mentioned in a question and assume it was the client that replied.
   * Fallback (TODO this function should be able to figure out who is speaking, maybe webhooks required or visual recognition)
   */
  indentifySpeaker() {
    // Alexia
    // Google Home
    // else
    return (!this.speaker) ? 'Panel' : this.speaker
  }

  /**
   * Decide whom to address the question to, depending on how often the other has been speaking (use random numbers, ask short questions to both)
   * @param text is prefixed by the speakers name and ended as a question by default.
   */
  orchestrateToSpeakers(text: string) {
    console.log(this.speaker + ' is addressed for another ' + this.repetitionsCount + ' time(s) before ',this.forcedSpeaker)
    // force 
    if(this.forcedSpeaker != 'Automatic'){
      this.speaker = this.forcedSpeaker 
    }
    // automatic
    else {
      // May still speak
      if(this.repetitionsCount > 0){
        this.repetitionsCount-- 
      } 
      // Switch to other speaker
      else {
        // reset repetitions
        this.repetitionsCount = this.repetitionOptions[Math.floor(Math.random() * this.repetitionOptions.length)]-1
        this.speaker = (this.speaker == this.wakeWords.alexia) ? this.wakeWords.google : this.wakeWords.alexia
      }
      
      console.log(this.speaker + 'is addressed for another ' + this.repetitionsCount + ' time(s) before ')
    }
    // Add some prefixes to make the conversation, less repetetiv before calling the wakeWords
    let prefix = this.prefixPhrases[Math.floor(Math.random() * this.prefixPhrases.length)]
    return prefix + ' ' + this.speaker + ', \n\n' + text.slice(0, -1) + '?'
  }

  forceSpeaker(value: string) {
    console.log(value)
    this.speaker = value
  }

  getExcuse(){
    return this.fallbackExcuses.splice(Math.floor(Math.random() * this.fallbackExcuses.length))[0]
  }

}

